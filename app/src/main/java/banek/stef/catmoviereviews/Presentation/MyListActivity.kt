package banek.stef.catmoviereviews.Presentation

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import banek.stef.catmoviereviews.Models.MovieResult
import banek.stef.catmoviereviews.Providers.FirestoreListener
import banek.stef.catmoviereviews.Providers.FirestoreProvider
import banek.stef.catmoviereviews.R
import com.google.firebase.auth.FirebaseAuth
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_my_list.*
import javax.inject.Inject

class MyListActivity : AppCompatActivity(), FirestoreListener {
    @Inject
    lateinit var firestoreProvider: FirestoreProvider
    private lateinit var mAuth: FirebaseAuth
    private lateinit var movieAdapter:MovieAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        firestoreProvider.register(this)
        setContentView(R.layout.activity_my_list)
        recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        movieAdapter = MovieAdapter(ArrayList(), applicationContext)
        recyclerView.adapter = movieAdapter
        mAuth = FirebaseAuth.getInstance()

        val swipeHandler = object : SwipeCallback(applicationContext) {
            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                firestoreProvider.removeMovie((p0 as ViewHolder).item!!.id)
                movieAdapter.items.removeAt(p0.adapterPosition)
                movieAdapter.notifyDataSetChanged()
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        firestoreProvider.getList()
    }


    override fun onList(result: ArrayList<MovieResult>) {
        movieAdapter.items.clear()
        movieAdapter.items.addAll(result)
        movieAdapter.notifyDataSetChanged()
    }

    override fun onRecomend(id1: Int, id2: Int, id3: Int) {
    }


    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(applicationContext, MovieActivity::class.java))
        finish()
    }
}
