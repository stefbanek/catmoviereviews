package banek.stef.catmoviereviews.Presentation

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.media.MediaPlayer
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import banek.stef.catmoviereviews.Auth.LoginActivity
import banek.stef.catmoviereviews.Models.Movie
import banek.stef.catmoviereviews.Models.MovieResult
import banek.stef.catmoviereviews.Providers.FirestoreListener
import banek.stef.catmoviereviews.Providers.FirestoreProvider
import banek.stef.catmoviereviews.Providers.MovieListener
import banek.stef.catmoviereviews.Providers.MovieProvider
import banek.stef.catmoviereviews.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_movie.*
import javax.inject.Inject

class MovieActivity : AppCompatActivity(), MovieListener, FirestoreListener {

    override fun onList(result: ArrayList<MovieResult>) {

    }

    override fun onRecomend(id1: Int, id2: Int, id3: Int) {
        movieProvider.getRecommendations(id1, id2, id3)
    }

    override fun onSearchResults(result: ArrayList<MovieResult>?) {
        navigation.isClickable = true
        movieAdapter.items.clear()
        searchView!!.isClickable = true
        movieAdapter.items.addAll(result!!)
        movieAdapter.notifyDataSetChanged()
        showProgress(false)
    }

    override fun onMovieResult(result: Movie?) {
    }

    override fun onRecommendationResult(result: ArrayList<MovieResult>?) {
        navigation.isClickable = true
        if(searchView != null){
            searchView!!.isClickable = true
        }
        movieAdapter.items.addAll(result!!)
        movieAdapter.notifyDataSetChanged()
        showProgress(false)
        refresh.isRefreshing=false
    }

    override fun onMultipleMovieResults(result: ArrayList<MovieResult>?) {
        navigation.isClickable = true
        if(searchView != null){
            searchView!!.isClickable = true
        }
        movieAdapter.items.addAll(result!!)
        movieAdapter.notifyDataSetChanged()
        showProgress(false)
        refresh.isRefreshing=false
    }

    @Inject
    lateinit var movieProvider: MovieProvider
    @Inject
    lateinit var firestoreProvider: FirestoreProvider
    private lateinit var mAuth: FirebaseAuth
    private var searchView: SearchView? = null
    private lateinit var animationDrawable: AnimationDrawable
    private lateinit var movieAdapter: MovieAdapter


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_now_playing -> {
                showProgress(true)
                movieAdapter.items.clear()
                movieAdapter.notifyDataSetChanged()
                movieProvider.getNowPlaying()
                navigation.isClickable = false
                searchView!!.isClickable = false
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_trending -> {
                showProgress(true)
                movieAdapter.items.clear()
                movieAdapter.notifyDataSetChanged()
                movieProvider.getTrending()
                navigation.isClickable = false
                searchView!!.isClickable = false
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_upcoming -> {
                showProgress(true)
                movieAdapter.items.clear()
                movieAdapter.notifyDataSetChanged()
                firestoreProvider.getRecomendations()
                navigation.isClickable = false
                searchView!!.isClickable = false
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        FirebaseApp.initializeApp(applicationContext)
        movieProvider.register(this)
        firestoreProvider.register(this)
        setContentView(R.layout.activity_movie)
        animationDrawable = login_progress.background as AnimationDrawable
        mAuth = FirebaseAuth.getInstance()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        movieAdapter = MovieAdapter(ArrayList(), applicationContext)
        recyclerView.adapter = movieAdapter
        val swipeHandler = object : SwipeCallback(applicationContext) {
            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                movieAdapter.notifyDataSetChanged()
                firestoreProvider.saveMovie((p0 as ViewHolder).item)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)
        refresh.setOnRefreshListener {
            when (navigation.selectedItemId) {
                R.id.navigation_now_playing -> {
                    showProgress(true)
                    movieAdapter.items.clear()
                    movieAdapter.notifyDataSetChanged()
                    movieProvider.getNowPlaying()
                    navigation.isClickable = false
                    searchView!!.isClickable = false
                }
                R.id.navigation_trending -> {
                    showProgress(true)
                    movieAdapter.items.clear()
                    movieAdapter.notifyDataSetChanged()
                    movieProvider.getTrending()
                    navigation.isClickable = false
                    searchView!!.isClickable = false
                }
                R.id.navigation_upcoming -> {
                    showProgress(true)
                    movieAdapter.items.clear()
                    movieAdapter.notifyDataSetChanged()
                    firestoreProvider.getRecomendations()
                    navigation.isClickable = false
                    searchView!!.isClickable = false
                }
            }
        }
        movieProvider.getNowPlaying()
        showProgress(true)
        navigation.isClickable = false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        searchView = menu!!.findItem(R.id.action_search).actionView as SearchView
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                if(p0!=null && p0.isNotEmpty()){
                    movieProvider.search(p0)
                    showProgress(true)
                    navigation.isClickable = false
                }
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if(p0!=null && p0.isNotEmpty()){
                    movieProvider.search(p0)
                    showProgress(true)
                    navigation.isClickable = false
                }
                return true
            }
        })
        searchView!!.setIconifiedByDefault(true)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.sign_out_action -> {
                mAuth.signOut()
                val aboutIntent = Intent(this@MovieActivity, LoginActivity::class.java)
                startActivity(aboutIntent)
                finish()
            }
            R.id.my_list_action ->{
                val aboutIntent = Intent(this@MovieActivity, MyListActivity::class.java)
                startActivity(aboutIntent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if(show){
            login_progress.visibility = View.VISIBLE
            animationDrawable.start()
        }
        else{
            login_progress.visibility = View.GONE
            animationDrawable.stop()
        }
    }


    override fun onDestroy() {
        val mp = MediaPlayer.create(this, R.raw.catscream)
        mp.start()
        mp.setOnCompletionListener {
            mp.release()
        }
        super.onDestroy()
    }
}
