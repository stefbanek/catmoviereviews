package banek.stef.catmoviereviews.Presentation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import banek.stef.catmoviereviews.Models.Actor
import banek.stef.catmoviereviews.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.cast_list_item.view.*
import java.lang.Exception

class CastAdapter(val ctx:Context, val items: MutableList<Actor>): RecyclerView.Adapter<CastAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(ctx).inflate(R.layout.cast_list_item, p0, false))
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.pic.setImageDrawable(null)
        try{
            Glide.with(ctx).load(item.picUri).into(holder.pic)
        }
        catch (ex:Exception){
            holder.pic.setImageResource(R.drawable.placeholdercat)
        }
        holder.nameA.text = item.name
        holder.role.text = item.role
    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val pic = view.actorImage
        val nameA = view.actorName
        val role = view.actorRole
    }
}

