package banek.stef.catmoviereviews.Presentation

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import banek.stef.catmoviereviews.Models.Movie
import banek.stef.catmoviereviews.Models.MovieResult
import banek.stef.catmoviereviews.Providers.MovieListener
import banek.stef.catmoviereviews.Providers.MovieProvider
import banek.stef.catmoviereviews.R
import com.bumptech.glide.Glide
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_movie_detail.*
import javax.inject.Inject
import android.net.Uri
import android.support.v7.widget.LinearLayoutManager
import android.widget.Button
import banek.stef.catmoviereviews.Providers.FirestoreProvider
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import java.lang.NullPointerException


class MovieDetailActivity : AppCompatActivity(), MovieListener {
    override fun onSearchResults(result: ArrayList<MovieResult>?) {
    }

    override fun onMultipleMovieResults(result: ArrayList<MovieResult>?) {
    }

    override fun onRecommendationResult(result: ArrayList<MovieResult>?) {
    }

    override fun onMovieResult(result: Movie?) {
        if(result==null){
            return
        }
        movie = result
        setUpLayout()
    }

    @Inject
    lateinit var movieProvider: MovieProvider
    @Inject
    lateinit var firestoreProvider: FirestoreProvider
    private lateinit var movie: Movie
    private lateinit var adapter: CastAdapter
    private val apiKey = "AIzaSyAt1DNGkA5O0E9TManVFucmpPlk2WDxHaM"
    private val player = YouTubePlayerSupportFragment.newInstance()
    private var mylist:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_movie_detail)
        movieProvider.register(this)
        movieProvider.getMovie(intent.getIntExtra("id", 0))
        setSupportActionBar(detail_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        cast_recycler.layoutManager = LinearLayoutManager(this)
        adapter = CastAdapter(this, ArrayList())
        cast_recycler.adapter = adapter

        mylist = intent.getBooleanExtra("list", false)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_fragment, player)
        transaction.commit()

    }

    private fun setUpLayout(){
        toolbar_layout.title = movie.title
        Glide.with(applicationContext).load(movie.posterUri).into(backdrop)
        if(movie.genres!=null){
            var genres = ""
            for(g in movie.genres!!){
                genres+="${g.toUpperCase()}  "
            }
            genres_text.text = genres
        }
        else{
            genres_text.text=getString(R.string.genres_unknown_string)
        }
        rating_text.text = movie.voteAvg.toString()
        duration_text.text = movie.runtime?:"I don't care!"
        status_text.text = movie.status
        original_title_text.text = movie.originalTitle
        release_date_text.text = movie.releaseDate
        overview_text.text = movie.overview
        page_link_button.setOnClickListener {
            val uri = Uri.parse(movie.homepageUri)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
        imdb_link_button.setOnClickListener {
            val uri = Uri.parse(movie.imdbUri)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
        budget_text.text = "${movie.budget} $"
        revenue_text.text = "${movie.revenue} $"

        if(movie.cast.size>=12){
            adapter.items.addAll(movie.cast.values.take(12).toMutableList())
        }
        else{
            adapter.items.addAll(movie.cast.values.toMutableList())
        }
        adapter.notifyDataSetChanged()

        var production = ""
        var countp = 0
        for(g in movie.productionCompanies){
                countp+=1
                if(countp>3){
                    break
                }
                production+="${g.toUpperCase()}  "
            }
        production_companies_text.text = production

        if(mylist){
            save_button.setBackgroundResource(R.color.colorPrimary)
            save_button.text = getString(R.string.remove_from_purr_string)
            save_button.setOnClickListener {
                firestoreProvider.removeMovie(movie.id)
            }
        }
        else{
            save_button.setBackgroundResource(R.color.colorAccent)
            save_button.text = getString(R.string.save_to_purrlist_string)
            save_button.setOnClickListener {
                firestoreProvider.saveMovie(MovieResult(movie.id, movie.releaseDate, movie.title, movie.backdropUri, movie.voteAvg))
            }
        }

        player.initialize(apiKey, object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(provider: YouTubePlayer.Provider, youTubePlayer: YouTubePlayer, b: Boolean) {
                try{
                    youTubePlayer.loadVideo(movie.ytID)
                    youTubePlayer.pause()
                }
                catch (e: NullPointerException){
                    e.printStackTrace()
                }
            }
            override fun onInitializationFailure(provider: YouTubePlayer.Provider, youTubeInitializationResult: YouTubeInitializationResult) {
                youTubeInitializationResult.getErrorDialog(this@MovieDetailActivity, 0).show()
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                navigateUpTo(Intent(this, MovieActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
