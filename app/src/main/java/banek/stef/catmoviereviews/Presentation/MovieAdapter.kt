package banek.stef.catmoviereviews.Presentation

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import banek.stef.catmoviereviews.Models.MovieResult
import banek.stef.catmoviereviews.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.movieresult_list_content.view.*

class MovieAdapter(val items:MutableList<MovieResult>, val ctx: Context): RecyclerView.Adapter<ViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(ctx).inflate(R.layout.movieresult_list_content, p0, false))
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.item = item
        holder.cv.setImageDrawable(null)
        Glide.with(ctx).load(item.backdropUri).into(holder.cv)
        holder.title.text = item.title
        holder.rating.text = "Purr rate: ${item.voteAverage}"
        holder.release.text = "Last meal: ${item.releaseDate}"

        holder.itemView.setOnClickListener {
            val i = Intent(ctx, MovieDetailActivity::class.java)
            i.putExtra("id", item.id)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            ctx.startActivity(i)
        }

    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    var item:MovieResult? = null
    val cv = view.cv
    val title = view.title_text
    val rating = view.rating
    val release = view.release
}