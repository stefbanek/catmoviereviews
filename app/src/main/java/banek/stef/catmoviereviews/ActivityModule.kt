package banek.stef.catmoviereviews

import banek.stef.catmoviereviews.Presentation.MovieActivity
import banek.stef.catmoviereviews.Presentation.MovieDetailActivity
import banek.stef.catmoviereviews.Presentation.MyListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    internal abstract fun contributeMovieActivity(): MovieActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMovieDetailActivity(): MovieDetailActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMyListActivity(): MyListActivity

    //TODO Add bindings for other sub-components here
}