package banek.stef.catmoviereviews.Models

data class Actor(val name:String, val picUri:String?, val role:String){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Actor) return false

        if (name != other.name) return false
        if (picUri != other.picUri) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + (picUri?.hashCode() ?: 0)
        return result
    }

}