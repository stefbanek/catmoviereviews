package banek.stef.catmoviereviews.Models
//API search movies
data class MovieResult(val id:Int, val releaseDate:String,
                       val title:String, val backdropUri: String?, val voteAverage:Number) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MovieResult) return false

        if (id != other.id) return false
        if (releaseDate != other.releaseDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + releaseDate.hashCode()
        return result
    }
}