package banek.stef.catmoviereviews.Models
//Api get movie details. Treba koristit i get videos i get reviews i get latest i get popular
data class Movie(val id:Int, val backdropUri: String?, val budget: Int, val genres: MutableList<String>?, val homepageUri: String?, val imdbUri:String?,
                 val originalTitle: String, val overview:String?, val posterUri: String?, val productionCompanies: MutableList<String>, val releaseDate: String
, val revenue: Int, val runtime:String?, val status: String, val title: String, val voteAvg: Number, val voteCnt: Int, val ytID:String?, val cast:Map<String, Actor>){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Movie) return false

        if (id != other.id) return false
        if (budget != other.budget) return false
        if (originalTitle != other.originalTitle) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + budget
        result = 31 * result + originalTitle.hashCode()
        return result
    }

}