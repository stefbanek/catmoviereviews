package banek.stef.catmoviereviews.Auth

import android.annotation.TargetApi
import android.support.v7.app.AppCompatActivity
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.util.Log
import android.widget.Toast
import banek.stef.catmoviereviews.Presentation.MovieActivity
import banek.stef.catmoviereviews.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser


import kotlinx.android.synthetic.main.activity_login.*

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity() {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private lateinit var mAuth: FirebaseAuth
    private lateinit var animationDrawable: AnimationDrawable
    private lateinit var loginDrawable: AnimationDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try{
            setContentView(R.layout.activity_login)
        }
        catch (e:Exception){
            e.printStackTrace()
        }
        FirebaseApp.initializeApp(applicationContext)
        animationDrawable = login_progress.background as AnimationDrawable
        loginDrawable = email_sign_in_button.background as AnimationDrawable
        loginDrawable.start()
        // Set up the login form.
        mAuth = FirebaseAuth.getInstance()
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        email_sign_in_button.setOnClickListener { attemptLogin() }
    }


    override fun onStart() {
        super.onStart()
        val currentUser = mAuth.currentUser
        if(currentUser!=null){
            startMainActivity(currentUser)
        }
    }



    fun startMainActivity(user: FirebaseUser){
        showProgress(false)
        val i = Intent(this, MovieActivity::class.java)
        i.putExtra("display", user.displayName)
        startActivity(i)
        finish()
    }



    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() {
        // Reset errors.
        email.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(passwordStr) && !isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.error_field_required)
            focusView = email
            cancel = true
        } else if (!isEmailValid(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            emailSignIn("pjesmarica123@gmail.com", "MojaLozinka1")
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return email == "Ivan"
    }

    private fun isPasswordValid(password: String): Boolean {
        return password == "Bukovac"
    }


    private fun emailSignIn(email:String, password: String){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = mAuth.currentUser
                    startMainActivity(user!!)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Sign in error", "signInWithEmail:failure", it.exception)
                    Toast.makeText(applicationContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                }
        }
    }
    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if(show){
            login_progress.visibility = View.VISIBLE
            animationDrawable.start()
        }
        else{
            login_progress.visibility = View.GONE
            animationDrawable.stop()
        }
    }

}
