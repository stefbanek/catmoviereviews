package banek.stef.catmoviereviews

import android.app.Application
import banek.stef.catmoviereviews.Providers.FirestoreProvider

import banek.stef.catmoviereviews.Providers.MovieProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class AppModule {
    @Singleton
    @Provides
    fun provideApi(app: Application): MovieProvider {
        return MovieProvider(app.applicationContext)
    }
    @Singleton
    @Provides
    fun provideFirestore(app: Application): FirestoreProvider {
        return FirestoreProvider(app.applicationContext)
    }
}