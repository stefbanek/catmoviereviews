package banek.stef.catmoviereviews.Providers

import android.content.Context
import android.widget.Toast
import banek.stef.catmoviereviews.Models.MovieResult
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.Exception
import kotlin.random.Random

class FirestoreProvider(val ctx:Context){
    val db = FirebaseFirestore.getInstance()
    val listeners = ArrayList<FirestoreListener>()

    fun saveMovie(m:MovieResult?){
        if(m==null){
            Toast.makeText(ctx, "Couldn't save movie", Toast.LENGTH_LONG).show()
            return
        }

        val map = HashMap<String, Any?>()
        map["id"] = m.id
        map["releaseDate"] = m.releaseDate
        map["title"] = m.title
        map["backdropUri"] = m.backdropUri
        map["voteAverage"] = m.voteAverage

        db.collection("movies").document(m.id.toString()).set(map)
            .addOnSuccessListener {
                Toast.makeText(ctx, "Meowie saved!", Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener {
                Toast.makeText(ctx, "Couldn't save meowie", Toast.LENGTH_LONG).show()
            }
    }

    fun removeMovie(id: Int){
        db.collection("movies").document(id.toString())
            .delete()
            .addOnSuccessListener { Toast.makeText(ctx, "Meowie deleted!", Toast.LENGTH_LONG).show()}
            .addOnFailureListener { Toast.makeText(ctx, "Couldn't delete meowie", Toast.LENGTH_LONG).show()}
    }

    fun getList(){
        db.collection("movies").get()
            .addOnSuccessListener { docs ->
                val res = ArrayList<MovieResult>()
                for(doc in docs){
                    val mr = mapToMR(doc.data)
                    if(mr!=null){
                        res.add(mr)
                    }
                }
                for(l in listeners){
                    l.onList(res)
                }
            }
            .addOnFailureListener { Toast.makeText(ctx, "Couldn't fetch purr list", Toast.LENGTH_LONG).show() }
    }

    fun getRecomendations(){
        db.collection("movies").get()
            .addOnSuccessListener {docs ->
                val res = MutableList(3) {-1}
                if(docs.size()<3){
                    return@addOnSuccessListener
                }
                val list = ArrayList<String>()
                for(doc in docs){
                    list.add(doc.id)
                }

                val n = list.count()
                var cnt = 0
                while (cnt<3){
                    val i = Random.nextInt(0, n)
                    if(res.contains(i))
                        continue
                    else{
                        res[cnt]=i
                        cnt++
                    }
                }
                for(l in listeners){
                    l.onRecomend(list[res[0]].toInt(), list[res[1]].toInt(), list[res[2]].toInt())
                }
            }
            .addOnFailureListener {
                Toast.makeText(ctx, "Couldn't get meowies", Toast.LENGTH_LONG).show()
            }
    }

    fun register(l :FirestoreListener){
        listeners.add(l)
    }

    private fun mapToMR(map: Map<String, Any>):MovieResult?{
        return try{
            val id = (map["id"] as Long).toInt()
            val backdropUri = map["backdropUri"] as String
            val releaseDate = map["releaseDate"] as String
            val title = map["title"] as String
            val voteAvg = (map["voteAverage"] as Long).toInt()
            return MovieResult(id, releaseDate, title, backdropUri, voteAvg)
        } catch (ex: Exception){
            null
        }
    }
}

interface FirestoreListener{
    fun onList(result: ArrayList<MovieResult>)
    fun onRecomend(id1:Int, id2:Int, id3:Int)
}