package banek.stef.catmoviereviews.Providers

import android.content.Context
import banek.stef.catmoviereviews.Models.Actor
import banek.stef.catmoviereviews.Models.Movie
import banek.stef.catmoviereviews.Models.MovieResult
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser

class MovieProvider(val ctx:Context):ApiListener {
    private val listeners = ArrayList<MovieListener>()
    private var apiProvider: ApiProvider = ApiProvider()

    init {
        apiProvider.register(this)
    }

    override fun onSearchResults(result: String) {
        val res = parseMovieResults(result)
        for(l in listeners){
            l.onSearchResults(res)
        }
    }

    override fun onMultipleMovieResults(result: MutableList<String>) {
        val res = ArrayList<MovieResult>()
        for(r in result){
            res.addAll(parseMovieResults(r))
        }
        for(l in listeners){
            l.onMultipleMovieResults(res)
        }
    }

    override fun onReccomendationResults(result: MutableList<String>) {
        val res = ArrayList<MovieResult>()
        for(r in result){
            res.addAll(parseMovieResults(r))
        }
        for(l in listeners){
            l.onRecommendationResult(res)
        }
    }

    override fun onMovieResult(result: MutableList<String>) {
        val mov = parseMovie(result)
        for(l in listeners){
            l.onMovieResult(mov)
        }
    }


    fun register(l:MovieListener){
        listeners.add(l)
    }

    fun search(query:String){
        apiProvider.search(query, ctx)
    }

    fun getNowPlaying(){
        apiProvider.getNowPlaying(ctx)
    }

    fun getUpcoming(){
        apiProvider.getUpcoming(ctx)
    }

    fun getTrending(){
        apiProvider.getTrending(ctx)
    }

    fun getMovie(id:Int){
        apiProvider.getMovie(ctx, id)
    }

    fun getRecommendations(id1: Int, id2: Int, id3: Int){
        apiProvider.getReccomendations(ctx, id1, id2, id3)
    }



    private fun parseMovieResults(input:String):ArrayList<MovieResult>{
        val res = ArrayList<MovieResult>()
        if(input.isEmpty())
            return res
        val parser: Parser = Parser.default()
        val stringBuilder = StringBuilder(input)
        try{
            val json: JsonObject = parser.parse(stringBuilder) as JsonObject
            val array = json.array<JsonObject>("results")
            if(json.int("total_results")==0 || array==null)
                return res

            for(s in array){
                val id = s.int("id")
                val releaseDate = s.string("release_date")
                val title = s.string("title")
                val backdropUri = "http://image.tmdb.org/t/p/w780" + s.string("backdrop_path")
                val voteAvg = s.int("vote_average")
                val mr = MovieResult(id!!, releaseDate!!, title!!, backdropUri, voteAvg!!)
                res.add(mr)
            }
            return res
        }
        catch (ex: Exception){
            return res
        }

    }

    private fun parseMovie(input:MutableList<String>): Movie?{
        val parser: Parser = Parser.default()
        try{
            var stringBuilder = StringBuilder(input[0])
            var json: JsonObject = parser.parse(stringBuilder) as JsonObject
            if(json.int("total_results")==0)
                return null
            val id = json.int("id")
            val backdropUri = "http://image.tmdb.org/t/p/w780" + json.string("backdrop_path")
            val budget = json.int("budget")
            val genres = ArrayList<String>()
            val g = json.array<JsonObject>("genres")
            for(gen in g!!){
                genres.add(gen.string("name")!!)
            }
            val homepageUri = json.string("homepage")
            val imdbUri = "https://www.imdb.com/title/" + json.string("imdb_id")
            val originalTitle = json.string("original_title")
            val overview = json.string("overview")
            val posterUri = "http://image.tmdb.org/t/p/w342" + json.string("poster_path")
            val productionCompanies = ArrayList<String>()
            val p = json.array<JsonObject>("production_companies")
            for(pro in p!!){
                productionCompanies.add(pro.string("name")!!)
            }
            val releaseDate = json.string("release_date")
            val revenue = json.int("revenue")
            val runint = json.int("runtime")
            val h = runint?.div(60)
            val m = runint?.rem(60)
            val runtime = "${h?:0} hours and ${m?:0} minutes"
            val status = json.string("status")
            val title = json.string("title")
            val voteAvg = json.int("vote_average")
            val voteCnt = json.int("vote_count")

            val ytId:String? = parseVideo(input[1])

            val cast = HashMap<String, Actor>()
            stringBuilder = StringBuilder(input[2])
            json = parser.parse(stringBuilder) as JsonObject
            val ar = json.array<JsonObject>("cast")
            var counter = 0
            for(s in ar!!){
                counter++
                if(counter==12){
                    break
                }
                val characte = s.string("character")
                val nam = s.string("name")
                val picUr = "http://image.tmdb.org/t/p/w185" + s.string("profile_path")
                cast[characte!!] = Actor(nam!!, picUr, characte)
            }

            val mov = Movie(id!!, backdropUri, budget!!, genres, homepageUri, imdbUri, originalTitle!!, overview, posterUri, productionCompanies, releaseDate!!, revenue!!, runtime, status!!, title!!, voteAvg!!, voteCnt!!, ytId, cast)
            return mov
        }
        catch (ex:java.lang.Exception){
            return null
        }

    }

    private fun parseVideo(input:String):String?{
        val parser: Parser = Parser.default()
        try {
            val stringBuilder = StringBuilder(input)
            val json: JsonObject = parser.parse(stringBuilder) as JsonObject
            val array = json.array<JsonObject>("results")
            if(json.int("total_results")==0 || array==null)
                return null
            for(s in array){
                val site = s.string("site")
                val type = s.string("type")
                if(site == "YouTube" && type == "Trailer"){
                    return s.string("key")
                }
            }
        }
        catch (ex:java.lang.Exception){
            return null
        }
        return null
    }
}

interface MovieListener{
    fun onSearchResults(result: ArrayList<MovieResult>?)
    fun onMovieResult(result: Movie?)
    fun onMultipleMovieResults(result:ArrayList<MovieResult>?)
    fun onRecommendationResult(result:ArrayList<MovieResult>?)
}