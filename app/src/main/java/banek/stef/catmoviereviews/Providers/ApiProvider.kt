package banek.stef.catmoviereviews.Providers

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley


class ApiProvider{
    private val apiKey = "44f2f8ca85b27a5e03cc45c8ef637aa6"
    private val listeners: MutableList<ApiListener> = ArrayList()
    private var multiResults = ArrayList<String>()
    private var movieResults = MutableList(3){""}
    private var resCnt = 0
    private var movieCnt = 0

    fun search(query:String, ctx: Context){
        val queue = Volley.newRequestQueue(ctx)
        val url = "https://api.themoviedb.org/3/search/movie?api_key=$apiKey&language=en-US&page=1&include_adult=true&query=${query.replace(" ", "%20")}"

        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                for(l in listeners){
                    l.onSearchResults(response)
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)
        queue.start()
    }

    fun getNowPlaying(ctx: Context){
        val queue = Volley.newRequestQueue(ctx)
        val url1 = "https://api.themoviedb.org/3/movie/now_playing?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=1&region=US"
        val url2 = "https://api.themoviedb.org/3/movie/now_playing?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=2&region=US"
        val url3 = "https://api.themoviedb.org/3/movie/now_playing?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=3&region=US"
        multiResults.clear()
        var stringRequest = StringRequest(
            Request.Method.GET, url1,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url2,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url3,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)
        queue.start()
    }

    fun getUpcoming(ctx: Context){
        val queue = Volley.newRequestQueue(ctx)
        multiResults.clear()
        val url1 = "https://api.themoviedb.org/3/movie/upcoming?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=1&region=US"
        val url2 = "https://api.themoviedb.org/3/movie/upcoming?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=2&region=US"
        val url3 = "https://api.themoviedb.org/3/movie/upcoming?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=3&region=US"

        var stringRequest = StringRequest(
            Request.Method.GET, url1,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url2,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url3,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)
        queue.start()
    }

    fun getTrending(ctx: Context){
        val queue = Volley.newRequestQueue(ctx)
        multiResults.clear()
        val url1 = "https://api.themoviedb.org/3/movie/popular?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=1&region=US"
        val url2 = "https://api.themoviedb.org/3/movie/popular?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=2&region=US"
        val url3 = "https://api.themoviedb.org/3/movie/popular?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=3&region=US"

        var stringRequest = StringRequest(
            Request.Method.GET, url1,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url2,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url3,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onMultipleMovieResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)
        queue.start()
    }


    fun getReccomendations(ctx: Context, id1:Int, id2: Int, id3:Int){
        val url1 = "https://api.themoviedb.org/3/movie/$id1/recommendations?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=1"
        val url2 = "https://api.themoviedb.org/3/movie/$id2/recommendations?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=1"
        val url3 = "https://api.themoviedb.org/3/movie/$id3/recommendations?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US&page=1"
        val queue = Volley.newRequestQueue(ctx)
        var stringRequest = StringRequest(
            Request.Method.GET, url1,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onReccomendationResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url2,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onReccomendationResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, url3,
            Response.Listener<String> { response ->
                resCnt += 1
                multiResults.add(response)
                if(resCnt == 3){
                    for(l in listeners){
                        l.onReccomendationResults(multiResults)
                        resCnt=0
                        multiResults.clear()
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)
        queue.start()

    }

    fun getMovie(ctx: Context, id:Int){
        val videoUrl = "https://api.themoviedb.org/3/movie/$id/videos?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US"
        val creditsUrl = "https://api.themoviedb.org/3/movie/$id/credits?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US"
        val url="https://api.themoviedb.org/3/movie/$id?api_key=44f2f8ca85b27a5e03cc45c8ef637aa6&language=en-US"
        val queue = Volley.newRequestQueue(ctx)
        var stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                movieCnt += 1
                movieResults.add(0,response)
                if(movieCnt == 3){
                    for(l in listeners){
                        l.onMovieResult(movieResults)
                        movieCnt=0
                        movieResults= MutableList(3){""}
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, videoUrl,
            Response.Listener<String> { response ->
                movieCnt += 1
                movieResults.add(1,response)
                if(movieCnt == 3){
                    for(l in listeners){
                        l.onMovieResult(movieResults)
                        movieCnt=0
                        movieResults= MutableList(3){""}
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)

        stringRequest = StringRequest(
            Request.Method.GET, creditsUrl,
            Response.Listener<String> { response ->
                movieCnt += 1
                movieResults.add(2, response)
                if(movieCnt == 3){
                    for(l in listeners){
                        l.onMovieResult(movieResults)
                        movieCnt=0
                        movieResults= MutableList(3){""}
                    }
                }
            },
            Response.ErrorListener {})

        queue.add(stringRequest)
        queue.start()

    }


    fun register(l:ApiListener){
        listeners.add(l)
    }


}

interface ApiListener{
    fun onSearchResults(result:String)
    fun onMultipleMovieResults(result:MutableList<String>)
    fun onReccomendationResults(result:MutableList<String>)
    fun onMovieResult(result:MutableList<String>)
}